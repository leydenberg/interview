import random
from typing import TypeVar

T = TypeVar("T")


def binary_search(arr: list[T], x: T) -> int:
    high = len(arr) - 1
    mid = 0
    low = 0
    while low <= high:
        mid = (high + low) // 2
        if arr[mid] < x:
            low = mid + 1
        elif arr[mid] > x:
            high = mid - 1
        else:
            return mid

    return -1


strings: list[str] = (
    random.choices(["a", "aa", "aaa", "b", "ab", "aab", "ba", "aba", ...], 2000) * 200
)

if __name__ == "__main__":
    result = []
    for query in strings:
        result.append(binary_search(strings, query))

# TODO
# Оценить сложность
# Оптимизировать код
