import requests


class SomeHelperService:
    def __init__(self):
        self.data = requests.get("https://foo.com").json()

    def do_something(self):
        ...


class SomeService:
    def __init__(self):
        self.other_service = SomeHelperService()

    def do_something(self):
        self.other_service.do_something()


if __name__ == "__main__":
    service = SomeService()
    service.do_something()

# TODO
# Что не так в этом коде?
# Как исправить?
