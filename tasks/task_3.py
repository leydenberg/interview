class A:
    def read(self):
        return "A"


class B:
    def read(self):
        return "B"


class C(A, B):
    pass


if __name__ == "__main__":
    c = C()
    print(c.read())

# TODO
# Что выведет output?
